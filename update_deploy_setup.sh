#!/bin/bash

set -e

DEFAULT_DEPLOY_SCRIPT_TAG="1.0.5"
DEFAULT_CONTROL_SCRIPT_TAG="3.0.1"
DEFAULT_DEPLOY_UTILS_TAG="1.0.6"
DEFAULT_DEPLOY_BACKUPS_TAG="1.0.2"

BASE_DIR="/home/deploy/deploy-assets"
USER="deploy"

DEPLOY_SCRIPT_DIR="deploy-script"
DEPLOY_SCRIPT_GIT="git@gitlab.com:CLARIN-ERIC/deploy-script.git"
CONTROL_SCRIPT_DIR="control-script"
CONTROL_SCRIPT_GIT="git@gitlab.com:CLARIN-ERIC/control-script.git"
DEPLOY_UTILS_DIR="deploy-utils"
DEPLOY_UTILS_GIT="git@gitlab.com:CLARIN-ERIC/deploy-utils.git"
DEPLOY_BACKUPS_DIR="backups"
DEPLOY_BACKUPS_GIT="git@gitlab.com:CLARIN-ERIC/backups.git"


HELP=0
RUN_AS_ROOT=0

#
# Process script arguments and parse all optional flags
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --control_script_tag)
        CONTROL_SCRIPT_TAG=$2
        shift
        ;;
    --deploy_script_tag)
        DEPLOY_SCRIPT_TAG=$2
        shift
        ;;
    --deploy_utils_tag)
        DEPLOY_UTILS_TAG=$2
        shift
        ;;
    --deploy_backups_tag)
        DEPLOY_BACKUPS_TAG=$2
        shift
        ;;
    -v|--verbose)
        set -x
        ;;
    -h|--help)
        HELP=1
        ;;
    *)
        echo "Unkown argument: $key"
        exit 1
        ;;
esac
shift # past argument or value
done

if [ "${CONTROL_SCRIPT_TAG}" == "" ]; then
    CONTROL_SCRIPT_TAG=${DEFAULT_CONTROL_SCRIPT_TAG}
fi
if [ "${DEPLOY_SCRIPT_TAG}" == "" ]; then
    DEPLOY_SCRIPT_TAG=${DEFAULT_DEPLOY_SCRIPT_TAG}
fi
if [ "${DEPLOY_UTILS_TAG}" == "" ]; then
    DEPLOY_UTILS_TAG=${DEFAULT_DEPLOY_UTILS_TAG}
fi
if [ "${DEPLOY_BACKUPS_TAG}" == "" ]; then
    DEPLOY_BACKUPS_TAG=${DEFAULT_DEPLOY_BACKUPS_TAG}
fi

update() {
    DIR="$1"
    TAG="$2"

    echo "######################################"
    echo "# Updating ${DIR} to version: ${TAG} #"
    echo "######################################"
    cd "${BASE_DIR}/${DIR}"
    sudo -u setup git fetch origin --depth=1 +refs/tags/${TAG}:refs/tags/${TAG}
    sudo -u setup git reset --hard tags/${TAG}
    sudo -u setup git gc --prune=all
}

install() {
    DIR="$1"
    GIT="$2"
    TAG="$3"

    echo "######################################"
    echo "Installing ${DIR} with version: ${TAG}"
    echo "######################################"
    cd "${BASE_DIR}"
    sudo -u setup git init "${DIR}"
    cd "${DIR}"
    sudo -u setup git remote add origin "${GIT}"
    sudo -u setup git fetch origin --depth=1 +refs/tags/${TAG}:refs/tags/${TAG}
    sudo -u setup git reset --hard tags/${TAG}
    sudo -u setup git gc --prune=all
}

symlink() {
    SRC="$1"
    DEST="$2"

    cd "/home/deploy"
    rm -f "${DEST}" && ln -s -f "${SRC}" "${DEST}"
}


#Check user
if [ "$(whoami)" != "${USER}" ]; then
    if [ "$(whoami)" == "root" ]; then
        echo "Running as root"
        RUN_AS_ROOT=1
        cd "/home/${USER}"
    else
        echo "This script must be run as user ${USER}"
        exit 1
    fi
fi

#Check dependencies
command -v jq >/dev/null 2>&1 || { echo >&2 "Jq is required. Pleas run `yum install jq`.  Aborting."; exit 1; }
command -v nc >/dev/null 2>&1 || { echo >&2 "Netcat is required. Pleas run `yum install nc`.  Aborting."; exit 1; }

#Check working dir
if [ "$(pwd)" != "/home/${USER}" ]; then
    echo "This script must be run from /home/${USER}"
    exit 1
fi

#Create base directory if it doesn't exist
if [ ! -d "${BASE_DIR}" ]; then
    echo "Creating ${BASE_DIR}"
    mkdir -p "${BASE_DIR}"
    chmod g+w "${BASE_DIR}"
fi

#
#Process deploy script
#
if [ -d "${BASE_DIR}/${DEPLOY_SCRIPT_DIR}" ]; then
    update "${DEPLOY_SCRIPT_DIR}" "${DEPLOY_SCRIPT_TAG}"
else
    install "${DEPLOY_SCRIPT_DIR}" "${DEPLOY_SCRIPT_GIT}" "${DEPLOY_SCRIPT_TAG}"
fi
symlink "${BASE_DIR}/${DEPLOY_SCRIPT_DIR}/deploy.sh" "/home/deploy/deploy.sh"
echo ""

#
#Process control script
#
if [ -d "${BASE_DIR}/${CONTROL_SCRIPT_DIR}" ]; then
    update "${CONTROL_SCRIPT_DIR}" "${CONTROL_SCRIPT_TAG}"
else
    install "${CONTROL_SCRIPT_DIR}" "${CONTROL_SCRIPT_GIT}" "${CONTROL_SCRIPT_TAG}"
fi
symlink "${BASE_DIR}/${CONTROL_SCRIPT_DIR}/control.sh" "/home/deploy/control.sh"
echo ""

#
#Process deploy script
#
if [ -d "${BASE_DIR}/${DEPLOY_UTILS_DIR}" ]; then
    update "${DEPLOY_UTILS_DIR}" "${DEPLOY_UTILS_TAG}"
else
    install "${DEPLOY_UTILS_DIR}" "${DEPLOY_UTILS_GIT}" "${DEPLOY_UTILS_TAG}"
fi
symlink "${BASE_DIR}/${DEPLOY_UTILS_DIR}/version.sh" "/home/deploy/version.sh"
symlink "${BASE_DIR}/${DEPLOY_UTILS_DIR}/list-projects.sh" "/home/deploy/list-projects.sh"
echo ""

#
#Process backup scripts
#
if [ -d "${BASE_DIR}/${DEPLOY_BACKUPS_DIR}" ]; then
    update "${DEPLOY_BACKUPS_DIR}" "${DEPLOY_BACKUPS_TAG}"
else
    install "${DEPLOY_BACKUPS_DIR}" "${DEPLOY_BACKUPS_GIT}" "${DEPLOY_BACKUPS_TAG}"
fi
echo ""
