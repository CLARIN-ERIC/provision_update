#!/bin/bash

#TODO: properly handle stderr as well
printf "Running wrapper.sh as user: %s\n" "$(whoami)"
USER="deploy"
if [ "$(whoami)" == "root" ]; then
    if [ "$#" -eq 0 ]; then
        curl -sSL https://gitlab.com/CLARIN-ERIC/provision_update/raw/master/update_deploy_setup.sh | sudo -i -u deploy bash 2>&1
    else
        curl -sSL https://gitlab.com/CLARIN-ERIC/provision_update/raw/master/update_deploy_setup.sh | sudo -i -u deploy bash -s -- $@ 2>&1
    fi
elif [ "$(whoami)" == "${USER}" ]; then
    if [ "$#" -eq 0 ]; then
        curl -sSL https://gitlab.com/CLARIN-ERIC/provision_update/raw/master/update_deploy_setup.sh | bash 2>&1
    else
        curl -sSL https://gitlab.com/CLARIN-ERIC/provision_update/raw/master/update_deploy_setup.sh | bash -s -- $@ 2>&1
    fi
else
    echo "This script must be run as user ${USER}"
    exit 1
fi



