# Install or update to the latest version of the deploy assets #

Run to following command to install or update the _latest_ version of the deploy assets:
```
curl -sSL https://gitlab.com/CLARIN-ERIC/provision_update/raw/master/wrapper.sh | bash
```

Run the following command to install or update a _specific_ version of the deploy assets:
```
curl -sSL https://gitlab.com/CLARIN-ERIC/provision_update/raw/master/wrapper.sh | bash -s -- --control_script_tag <tag value>
```

Where:
* `<tag value>` is replaced with the actual value.

## Install remotely

Replace `<server>` in the following examples with:
* a name corresponding to an entry in your local ssh config file 
* or with a string containing all the parameters to make the ssh connection.

Note: the following examples work for any user that can sudo to the deploy user, if you connect as the `deploy` user, you can omit the `sudo -u deploy` part.

Run to following command to install or update the _latest_ version of the deploy assets:
```
ssh <server> -t "curl -sSL https://gitlab.com/CLARIN-ERIC/provision_update/raw/master/wrapper.sh | sudo -u deploy bash"
```

Run the following command to install or update a _specific_ version of the deploy assets:
```
ssh <server> -t "curl -sSL https://gitlab.com/CLARIN-ERIC/provision_update/raw/master/wrapper.sh | sudo -u deploy bash -s -- --control_script_tag <tag value>"
```

Where:
* `<tag value>` is replaced with the actual value.

